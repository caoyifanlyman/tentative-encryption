/**This is the final Java project in CSC 127A fall semester.
 * 
 * @author: Huiyu Wang (Scarlett Wang)
 * 
 * My partner: Shane Charles Jones
 */

public class Encryption127A {
	
	//These are instance variables:
	
	private String str;           //两个实例变量
	private int[] array; 
	
	//This constructor initializes the instance variables.
	public Encryption127A (String original, int[] newKeys) {    //实例变量初始化
		str=original;    
		array=newKeys;     
	}

	//Convert the original string into a cipher to prevent unauthorized access.
	public String encrypt() {        
	char[] chars=new char[str.length()];            //建造一个新的char数组，用来重新安放String字符的位置
	String wrong="";                               // 不满足前提条件的情况下的排除还原
	
	//check the array is valid or not 
	if (array.length<2||array.length>16){          //array长度不在2到16inclusive这个范围只见的话还原wrong
		return wrong;
	}
	
	for(int i=0; i<=array.length-1; i++) {       //同理，array里任意的element不在1到100inclusive这个范围的时候，还原wrong
		if (array[i]<1||array[i]>100){ 
			return wrong;
		}
	}
	                                            /**
	                                             * 以上是前提条件
	                                             */

	for(int k=0; k<=str.length()-1&&k<=chars.length-1; k++){ //循环开始，设出的index k可用于String和新建字符数组char;因为两者长度一样
        char code=str.charAt(k);                             //任意取出String里的任意字符
        int position=str.indexOf(code); // This is not correct. indexOf() always returns the first occurance of the character in the string. position is actually equal to k.                     //记录下这个字符在String里第一次出现的位置；因为每个字符的移动是从它在String里的原始位置开始的
        int shiftTimes=0;                                    //这个是移动的次数，将要和array里的任何element相等

        // What does this loop do? You should probably follow your section leader's advice. 
        // 1. count the number of shiftTimes, shiftTimes = array[code % array.length]
        // 2. left wrapp-looping through the str, to determine if the char on that position is in the same group, if so, shiftTimes--;
        // 3. do step 2 until shiftTimes == 0
        // 4. clear shiftTimes to 0 , k++
        // Hint: You may want to record another array of all the group info about str, whose element on each position = str.charAt(k) % str.length.

	    for(int j=0; j<=array.length-1; j++){                   //进行group number和shiftTimes的判断
	    	if(code % array.length!=j){                           //如果group number算下来不等于array 的index number，还原错误
	    		return wrong;
	    	}
	    	if(shiftTimes!=array[j]){                          //如果移动的次数不等于array里面的元素；也是错的。
	    		return wrong; // Did you increment shiftTimes?
	    	}
	    }
	   	for(int x=position;x>=0&&x<=str.length()-1;x++)  {     /**这是移位的循环，让x＝position是因为每个字符的移动是从它在String里的原始位置开始的；但这部分很困难，因为移动次数和
	                                                           * 字符组号跟array息息相关；但我不知道怎么将这个逻辑连接起来；我熬夜熬了3天也想不出具体的逻辑。
	                                                           * 
	                                                           */

		  //TODO 
	   	}
	   }
	   return str;
	}	

    //Decode encrypted string back to the original message
	public String decrypt(String encrypt){

		return str;
	}
}



